
return function (rules)
  local goblin = rules:new_creature("Goblin", 1, 1)
  goblin:give_indestructible()

  local bear = rules:new_creature("Brown Bear", 2, 2)
  bear:give_wither()

  return goblin, bear
end

