
return function (rules)
  local goblin = rules:new_creature("Goblin", 1, 1)
  local bear = rules:new_creature("Brown Bear", 2, 2)

  return goblin, bear
end

