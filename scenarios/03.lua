
return function (rules)
  local goblin = rules:new_creature("Goblin", 1, 1)
  goblin:place_counter(rules:create_pp_counter())
  goblin:place_counter(rules:create_pp_counter())

  local bear = rules:new_creature("Brown Bear", 2, 2)

  return goblin, bear
end

