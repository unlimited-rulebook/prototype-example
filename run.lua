
package.path = "lib/?.lua;lib/?/init.lua;" .. package.path

local RULE_MODULES = { 'rules' }

local RULESETS = {
  'creature', 'damage', 'fight', 'indestructible', 'counter',
  'pp_counter', 'mm_counter', 'wither'
}

local rules = require 'ur-proto' (RULE_MODULES, RULESETS)
local scenario = require('scenarios.' .. (...))

local attacking_creature, blocking_creature = scenario(rules)

print()
print(attacking_creature.description)
print(blocking_creature.description)

print()
print("Fighting...")

attacking_creature:fight(blocking_creature)

print()
print(attacking_creature.description)
print(blocking_creature.description)

print()
print(("Is %s dead? %s"):format(attacking_creature.name,
                                tostring(attacking_creature:is_dead())))
print(("Is %s dead? %s"):format(blocking_creature.name,
                                tostring(blocking_creature:is_dead())))

