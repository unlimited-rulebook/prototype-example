
return function (ruleset)

  local r = ruleset.record

  r:new_property('damaged', { amount = 0 })

  function ruleset.define:get_damage(e)
    function self.when()
      return r:is(e, 'creature') and not r:is(e, 'damaged')
    end
    function self.apply()
      return 0
    end
  end

  function ruleset.define:get_damage(e)
    function self.when()
      return r:is(e, 'creature') and r:is(e, 'damaged')
    end
    function self.apply()
      return r:get(e, 'damaged', 'amount')
    end
  end

  function ruleset.define:is_dead(e)
    self.compose = true
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply(super)
      return super() or e:has_lethal_damage()
    end
  end

  function ruleset.define:has_lethal_damage(e)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      local damage = r:get(e, 'damaged', 'amount') or 0
      return damage >= e.toughness
    end
  end

  function ruleset.define:take_damage(e, amount)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      local total = (r:get(e, 'damaged', 'amount') or 0) + amount
      r:set(e, 'damaged', 'amount', total)
    end
  end

  function ruleset.define:cause_damage(source, e, amount) -- luacheck: no unused
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      e:take_damage(amount)
    end
  end

  function ruleset.define:get_description(e)
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and r:is(e, 'damaged')
    end
    function self.apply(super)
      return super() .. ("\n  Has %d damage"):format(e.damage)
    end
  end

  local dmg_description_rule = ruleset:get_last_rule()

  function ruleset.define:precedes(e1, e2)
    local greater = (e1 == dmg_description_rule)
    local smaller = (e2 == dmg_description_rule)
    function self.when()
      return greater or smaller
    end
    function self.apply()
      return greater or not smaller
    end
  end

end

