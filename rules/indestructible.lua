
return function (ruleset)

  local r = ruleset.record

  r:new_property('indestructible', {})

  function ruleset.define:give_indestructible(e)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      r:set(e, 'indestructible', {})
    end
  end

  function ruleset.define:has_lethal_damage(e)
    function self.when()
      return r:is(e, 'creature') and r:is(e, 'indestructible')
    end
    function self.apply()
      return false
    end
  end

  function ruleset.define:get_description(e)
    self.compose = true
    function self.when()
      return r:is(e, 'indestructible')
    end
    function self.apply(super)
      return super() .. "\n  Indestructible"
    end
  end

end

