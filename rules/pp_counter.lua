
return function (ruleset)

  local r = ruleset.record

  r:new_property('+/+', {})

  function ruleset.define:create_pp_counter()
    function self.when()
      return true
    end
    function self.apply()
      local counter = ruleset:new_entity()
      r:set(counter, '+/+', {})
      return counter
    end
  end

  function ruleset.define:get_pp_counters(e)
    local count = 0
    for _, counter in ipairs(e.counters) do
      if r:is(counter, '+/+') then
        count = count + 1
      end
    end
    function self.when()
      return true
    end
    function self.apply()
      return count
    end
  end

  function ruleset.define:get_power(e)
    local count = e.pp_counters
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and count > 0
    end
    function self.apply(super)
      return super() + count
    end
  end

  function ruleset.define:get_toughness(e)
    local count = e.pp_counters
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and count > 0
    end
    function self.apply(super)
      return super() + count
    end
  end

  function ruleset.define:get_description(e)
    local count = e.pp_counters
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and count > 0
    end
    function self.apply(super)
      return super() .. ("\n  Has %dx +1/+1 counters"):format(count)
    end
  end

end

