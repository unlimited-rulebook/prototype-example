
return function (ruleset)

  local r = ruleset.record

  r:new_property('wither', {})

  function ruleset.define:give_wither(e)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      r:set(e, 'wither', {})
    end
  end

  function ruleset.define:cause_damage(source, e, amount)
    function self.when()
      return r:is(e, 'creature') and r:is(source, 'wither')
    end
    function self.apply()
      for _ = 1, amount do
        e:place_counter(e.create_mm_counter())
      end
    end
  end

  function ruleset.define:get_description(e)
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and r:is(e, 'wither')
    end
    function self.apply(super)
      return super() .. "\n  Wither"
    end
  end

end

