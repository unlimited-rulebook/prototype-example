
return function (ruleset)

  local r = ruleset.record

  function ruleset.define:fight(e1, e2)
    function self.when()
      return r:is(e1, 'creature') and r:is(e2, 'creature')
    end
    function self.apply()
      local power1, power2 = e1.power, e2.power
      e1:cause_damage(e2, power1)
      e2:cause_damage(e1, power2)
    end
  end

end

