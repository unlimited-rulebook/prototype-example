
return function (ruleset)

  local r = ruleset.record

  r:new_property('-/-', {})

  function ruleset.define:place_counter(e, counter)
    local pp_counter
    for _, other_counter in ipairs(e.counters) do
      if r:is(other_counter, '+/+') then
        pp_counter = other_counter
        break
      end
    end
    function self.when()
      return r:is(counter, '-/-') and pp_counter
    end
    function self.apply()
      r:clear_all(pp_counter)
      r:clear_all(counter)
    end
  end

  function ruleset.define:create_mm_counter()
    function self.when()
      return true
    end
    function self.apply()
      local counter = ruleset:new_entity()
      r:set(counter, '-/-', {})
      return counter
    end
  end

  function ruleset.define:get_mm_counters(e)
    local count = 0
    for _, counter in ipairs(e.counters) do
      if r:is(counter, '-/-') then
        count = count + 1
      end
    end
    function self.when()
      return true
    end
    function self.apply()
      return count
    end
  end

  function ruleset.define:get_power(e)
    local count = e.mm_counters
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and count > 0
    end
    function self.apply(super)
      return super() - count
    end
  end

  function ruleset.define:get_toughness(e)
    local count = e.mm_counters
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and count > 0
    end
    function self.apply(super)
      return super() - count
    end
  end

  function ruleset.define:get_description(e)
    local count = e.mm_counters
    self.compose = true
    function self.when()
      return r:is(e, 'creature') and count > 0
    end
    function self.apply(super)
      return super() .. ("\n  Has %dx -1/-1 counters"):format(count)
    end
  end

end

