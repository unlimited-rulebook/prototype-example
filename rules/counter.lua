
return function (ruleset)

  local r = ruleset.record

  r:new_property('counter', { placed_on = false })

  function ruleset.define:place_counter(e, counter)
    function self.when()
      return true
    end
    function self.apply()
      r:set(counter, 'counter', { placed_on = e })
    end
  end

  function ruleset.define:get_counters(e)
    function self.when()
      return true
    end
    function self.apply()
      return r:where('counter', { placed_on = e })
    end
  end

end

