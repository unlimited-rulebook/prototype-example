
return function (ruleset)

  local r = ruleset.record

  r:new_property('creature', { power = 1, toughness = 1 })

  function ruleset.define:new_creature(name, power, toughness)
    function self.when()
      return true
    end
    function self.apply()
      local e = ruleset:new_entity()
      r:set(e, 'named', { name = name })
      r:set(e, 'creature', { power = power, toughness = toughness })
      return e
    end
  end

  function ruleset.define:get_power(e)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      return r:get(e, 'creature', 'power')
    end
  end

  function ruleset.define:get_toughness(e)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      return r:get(e, 'creature', 'toughness')
    end
  end

  function ruleset.define:is_dead(e)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      return e.toughness <= 0
    end
  end

  function ruleset.define:get_description(e)
    function self.when()
      return r:is(e, 'creature')
    end
    function self.apply()
      return ("%s (%d/%d creature)"):format(e.name, e.power, e.toughness)
    end
  end

end

